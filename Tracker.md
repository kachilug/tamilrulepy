| No       | Tasks | Issue | Status | Resource |
| -------- | ------- | ------- | ------- | ------- |
| 1        | Setup a tracker | NA | Completed | Parameshwar Arunachalam |
| 2        | Update all tasks to tracker | NA | Completed | Parameshwar Arunachalam |
| 3        | Testing dependency of data file in bloom filter | [Issue 1](https://gitlab.com/kachilug/tamilrulepy/-/issues/1)| In Progress | Dhilip |
| 4        | Complete sharing punartchi rules | NA | Completed | Sathiyaraj thangasamy |
| 5        | Writing grammar rule in simple plain format \- Meymayakkam | [Issue 2](https://gitlab.com/kachilug/tamilrulepy/-/issues/2) | In Progress | Parameshwar Arunachalam |
| 6        | Writing grammar rule in simple plain format \- Word Starting | [Issue 3](https://gitlab.com/kachilug/tamilrulepy/-/issues/3) | Pending  |  |
| 7        | Writing grammar rule in simple plain format \- Word Ending | [Issue 4](https://gitlab.com/kachilug/tamilrulepy/-/issues/4) | Pending |  |
| 8        | Test out test cases for 1 rule to check TDD | [Issue 5](https://gitlab.com/kachilug/tamilrulepy/-/issues/5) | Pending | |
| 9        | Package the existing contents to be ready for distribution | [Issue 6](https://gitlab.com/kachilug/tamilrulepy/-/issues/6) | Pending | |
| 10       | Start writing wiki for the current code base | [Issue 7](https://gitlab.com/kachilug/tamilrulepy/-/issues/7) | Pending | |