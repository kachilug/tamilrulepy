# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys
sys.path.insert(0, os.path.abspath('..'))

project = 'Tolkapy'
copyright = 'Made with Love by Opensource Enthusiasist'
author = 'Dr.Sathiyaraj Thangasamy" <sathiyarajt@skacas.ac.in>, Parameshwar Arunachalam <parameshwar273@gmail.com>, Boopalan S <boopalan28012003@gmail.com>, Hariharan Umapathi <smarthariharan28@gmail.com>, Infofarmer <tha.uzhavan@gmail.com>, Syed Jafer K <contact.syedjafer@gmail.com>, Kamalakkannan P <kkannan.ps.2004@gmail.com>'
release = 'V0.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = ['sphinx.ext.autodoc']

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']
